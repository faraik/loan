<?php

declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

error_reporting(E_ALL);

$env = 'dev';

/**
 * Set the error handler
 */
$whoops = new Whoops\Run();

if ($env !== 'prod') {
    $whoops->pushHandler(new Whoops\Handler\PrettyPageHandler);
} else {
    $whoops->pushHandler(function($e) {
        echo 'TODO:: User friendly error message';
    });
}
$whoops->register();

/* @var \Symfony\Component\HttpFoundation\Request */
$request = new \Symfony\Component\HttpFoundation\Request($_GET, $_POST, [], $_COOKIE, $_FILES, $_SERVER);
/* @var \Symfony\Component\HttpFoundation\Response */
$response = new \Symfony\Component\HttpFoundation\Response();

$routeDefinitionCallback = function (\FastRoute\RouteCollector $r) {
    $routes = include('Routes.php');
    foreach ($routes as $route) {
        $r->addRoute($route[0], $route[1], $route[2]);
    }
};

$dispatcher = \FastRoute\simpleDispatcher($routeDefinitionCallback);

$routeInfo = $dispatcher->dispatch($request->getMethod(), $request->getRequestUri());

switch ($routeInfo[0]) {
    case \FastRoute\Dispatcher::NOT_FOUND:
        $response->setContent('404 Page not found');
        $response->setStatusCode(404);
        break;
    case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $response->setContent('405 Method not allowed');
        $response->setStatusCode(405);
        break;
    case \FastRoute\Dispatcher::FOUND:
        $className = $routeInfo[1][0];
        $method = $routeInfo[1][1];
        $vars = $routeInfo[2];

        $className = new $className($response);
        $response = $className->$method($vars);
        break;
}
$response->send();
