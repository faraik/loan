<?php
namespace Loan\Application\Controller;

use Symfony\Component\HttpFoundation\Response;

class IndexController
{
    private $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function index() : Response
    {
        // TODO:: Interest earnings calculation report. Maybe just send JSON response browser
        $this->response->headers->set('Content-Type', 'application/json');
        $this->response->setContent(json_encode([]));
        $this->response->setStatusCode(200);
        return $this->response;
    }

}