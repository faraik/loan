<?php

namespace Loan\Domain\Model\Investor;

use Money\Money;

final class VirtualWallet
{
    /**
     * @var Money
     */
    private $balance;

    public function __construct(Money $balance)
    {
        $this->balance = $balance;
    }

    public function getBalance() : Money
    {
        return $this->balance;
    }

    public function setBalance(Money $balance)
    {
        $this->balance = $balance;
    }

    public static function fromString($balance) : VirtualWallet
    {
        if (!is_numeric($balance)) {
            throw new \InvalidArgumentException('Argument supplied for balance is not numeric');
        }

        return new self(Money::GBP($balance));
    }

}