<?php


namespace Loan\Domain\Model\Investor;

use Money\Money;

final class InterestEarned
{
    /** @var Money **/
    private $interestEarned;

    public function __construct(Money $interestEarned)
    {
        $this->interestEarned = $interestEarned;
    }

    public static function fromString($interestEarned)
    {
        if (!is_numeric($interestEarned)) {
            throw new \InvalidArgumentException("Argument supplied for 'Interest earned' is not numeric");
        }

        return new self(Money::GBP($interestEarned));
    }




}