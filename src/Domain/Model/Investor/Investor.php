<?php


namespace Loan\Domain\Model\Investor;

use Loan\Domain\Model\Investor\Exception\InsufficientFundsException;
use Loan\Domain\Model\Loan\Exception\ExceedsMaxInvestmentException;
use Loan\Domain\Model\Loan\Exception\TrancheNotFoundException;
use Loan\Domain\Model\Loan\Loan;
use Money\Money;
use Ramsey\Uuid\Uuid;

class Investor
{
    private $id;

    private $name;

    private $virtualWallet;

    private $interestEarned;

    public function __construct($name, VirtualWallet $wallet, Money $interestEarned)
    {
        $this->id = Uuid::uuid4()->toString();
        $this->setName($name);
        $this->virtualWallet = $wallet;
        $this->interestEarned = $interestEarned;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getVirtualWallet() : VirtualWallet
    {
        return $this->virtualWallet;
    }

    public function getInterestEarned() : Money
    {
        return $this->interestEarned;
    }

    public function setInterestEarned(Money $interest)
    {
        $this->interestEarned = $interest;
    }

    /**
     * Invest in loan
     * @param Loan $loan
     * @param string $trancheName
     * @param Money $amount
     * @param \DateTime $actionDate
     * @return bool
     * @throws InsufficientFundsException
     * @throws TrancheNotFoundException
     * @throws ExceedsMaxInvestmentException
     */
    public function invest(Loan $loan, string $trancheName, Money $amount, \DateTime $actionDate) : bool
    {
        if ($this->virtualWallet->getBalance()->lessThan($amount)) {
            throw new InsufficientFundsException('You have insufficient funds to make investment');
        }
        $tranches = $loan->getTranches();
        $tranche = $tranches->getTrancheByName($trancheName);

        $tranche->processInvestment($amount, self::getName(), $actionDate);

        // reduce virtual wallet balance after investment made
        $newWalletBalance = $this->virtualWallet->getBalance()->subtract($amount);
        $this->virtualWallet->setBalance($newWalletBalance);
        return true;
    }

    private function setName($name)
    {
        $name = trim($name);
        if (!$name) {
            throw new \InvalidArgumentException('name');
        }
        $this->name = $name;
    }

}