<?php


namespace Loan\Domain\Model\Service;


interface InterestCalculatorInterface
{
    public function calculate();
}