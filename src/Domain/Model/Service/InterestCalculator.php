<?php


namespace Loan\Domain\Model\Service;


use Loan\Domain\Model\Investor\Investor;
use Loan\Domain\Model\Loan\Loan;
use Loan\Domain\Model\Loan\Tranche;
use Money\Money;

class InterestCalculator implements InterestCalculatorInterface
{
    private $dateFrom;

    private $dateTo;

    private $loan;

    public function __construct(\DateTime $dateFrom, \DateTime $dateTo, Loan $loan)
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->loan = $loan;
    }

    public function calculate()
    {
        $tranches = $this->loan->getTranches();

        /* @var Tranche $tranche */
        foreach ($tranches as $tranche) {
            //TODO:: Improve by implementing investments against tranche as a collection
            $investments = $tranche->getInvestments();
            foreach ($investments as $investorId => $transactionDetails) {
                /* @var Investor $investor */
                $investor = $transactionDetails['investor'];
                $transactions = $this->getTransactionsByDateRange($this->dateFrom, $this->dateTo, $transactionDetails);
                $earnings = $this->calculateInterestEarnings($transactions, $tranche->getMonthlyInterest());
                $newInterestEarnedBalance = $investor->getInterestEarned()->add($earnings);
                $investor->setInterestEarned($newInterestEarnedBalance);
            }
        }
    }

    private function getTransactionsByDateRange(\DateTime $dateFrom, \DateTime $dateTo, $transactionDetails) : array
    {
//        //TODO:: Process data structure like the following to return relevant transactions:
//        $transactionDetails = [
//          ['amountInvested' => 100, 'actionDate' => DateTime],
//          ['amountInvested' => 10, 'actionDate' => DateTime],
//          ['amountInvested' => 5, 'actionDate' => DateTime]
//        ];

    }

    private function calculateInterestEarnings($transactions, int $monthlyInterest) : Money
    {
        //TODO:: WIP
        /* @var Money $runningTotal */
        $runningTotal = Money::GBP(0);
        $interest = $monthlyInterest / 100;

        foreach ($transactions as $transaction) {
            /* @var Money $amountInvested */
            $amountInvested = $transaction['amountInvested'];
            $earning = $amountInvested->multiply($interest);
            $runningTotal = $runningTotal->add($earning);
        }
        return $runningTotal;
    }
}