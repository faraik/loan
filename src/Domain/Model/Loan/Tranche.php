<?php

namespace Loan\Domain\Model\Loan;

use Loan\Domain\Model\Loan\Exception\ExceedsMaxInvestmentException;
use Money\Money;

class Tranche
{
    private $name;

    private $maximumInvestment;

    /** @var Money */
    private $totalAmountInvested;

    private $monthlyInterest;

    public function __construct(string $name, Money $maximumInvestment, int $monthlyInterest)
    {
        $this->setName($name);
        $this->maximumInvestment = $maximumInvestment;
        $this->monthlyInterest = $monthlyInterest;
        $this->totalAmountInvested = Money::GBP(0);
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getMaximumInvestment() : Money
    {
        return $this->maximumInvestment;
    }

    public function getMonthlyInterest() : int
    {
        return $this->monthlyInterest;
    }

    public function getTotalAmountInvested() : Money
    {
        return $this->totalAmountInvested;
    }

    public function processInvestment(Money $amount, string $investorName, \DateTime $actionDate)
    {
        $totalAmountInvestedValue = $this->totalAmountInvested->getAmount();
        $newInvestAmountValue = $amount->getAmount();
        $maximumInvestmentValue = $this->maximumInvestment->getAmount();

        if (((int)$totalAmountInvestedValue + (int)$newInvestAmountValue) > (int)$maximumInvestmentValue) {
            throw new ExceedsMaxInvestmentException('Exceeds Max Allowed Investment');
        }

        $this->totalAmountInvested = $this->totalAmountInvested->add($amount);
    }

    private function setName($name)
    {
        $name = trim($name);
        if (!$name) {
            throw new \InvalidArgumentException('name');
        }
        $this->name = $name;
    }



}