<?php


namespace Loan\Domain\Model\Loan;


interface TranchesCollectionInterface
{
    public function getTrancheClassName() : string;

    public function getTrancheByName(string $trancheName);

    public function trancheExists(string $trancheName) : bool;

}