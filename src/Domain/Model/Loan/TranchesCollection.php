<?php


namespace Loan\Domain\Model\Loan;


use Loan\Domain\Model\Loan\Exception\InvalidTrancheException;
use Loan\Domain\Model\Loan\Exception\TrancheNotFoundException;

class TranchesCollection implements TranchesCollectionInterface
{
    private $trancheClassName;
    private $tranches = [];

    /**
     * Creates a new typed collection of tranches.
     * @throws InvalidTrancheException
     */
    public function __construct(string $trancheClassName, array $tranches = [] )
    {
        $this->trancheClassName = $trancheClassName;
        $this->setupCollection($tranches, $trancheClassName);
    }

    public function getTrancheClassName(): string
    {
        return $this->trancheClassName;
    }

    /**
     * @throws TrancheNotFoundException
     */
    public function getTrancheByName(string $trancheName) : Tranche
    {
        if(!array_key_exists($trancheName, $this->tranches))
        {
            throw new TrancheNotFoundException( 'Name: ' . $trancheName );
        }

        return $this->tranches[$trancheName];
    }

    public function trancheExists(string $trancheName) : bool
    {
        if(array_key_exists($trancheName, $this->tranches))
        {
            return true;
        }

        return false;
    }

    /**
     * @throws InvalidTrancheException
     */
    private function setupCollection(array $tranches, $trancheClassName)
    {
        /* @var Tranche $tranche */
        foreach($tranches as $tranche)
        {
            if(!($tranche instanceof $trancheClassName))
            {
                throw new InvalidTrancheException('Expected instance of Tranche');
            }
            $this->tranches[$tranche->getName()] = $tranche;
        }
    }
}