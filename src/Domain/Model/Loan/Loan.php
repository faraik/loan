<?php


namespace Loan\Domain\Model\Loan;


use DateTime;

class Loan
{
    /* @var DateTime */
    private $startDate;

    /* @var DateTime */
    private $endDate;

    /* @var TranchesCollection */
    private $tranches;

    public function __construct(\DateTime $startDate, \DateTime $endDate)
    {
        $this->startDate = $startDate;
        $this->setEndDate($endDate);

    }

    public function getStartDate() : DateTime
    {
        return $this->startDate;
    }

    public function getEndDate() : DateTime
    {
        return $this->endDate;
    }

    /**
     * @param Tranches[] $tranches
     * @throws Exception\InvalidTrancheException
     */
    public function setTranches(array $tranches)
    {
        $this->tranches = new TranchesCollection(Tranche::class, $tranches);
    }

    public function getTranches() : TranchesCollection
    {
        return $this->tranches;
    }

    private function setEndDate(\DateTime $endDate)
    {
        if ($endDate < $this->startDate) {
            throw new \InvalidArgumentException('loan end date cannot be in the past to start date');
        }
        $this->endDate = $endDate;
    }

}