<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Loan\Domain\Model\Investor\Investor;
use Loan\Domain\Model\Investor\VirtualWallet;
use Loan\Domain\Model\Loan\Loan;
use Loan\Domain\Model\Loan\Tranche;
use Money\Money;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{

    private $investors;

    /* @var Loan */
    private $loan;

    private $outcome;
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->investors = [];
    }

    /**
     * @Given the following investors exist:
     */
    public function theFollowingInvestorsExist(TableNode $table)
    {
        $hash = $table->getHash();

        foreach ($hash as $row) {
            $this->investors[trim($row['name'])] = new Investor(
                trim($row['name']),
                VirtualWallet::fromString($row['virtual_wallet']),
                Money::GBP((int)$row['interest_earned'])
                );
        }
    }

    /**
     * @Given there is a loan with start date of :arg1 and end date of :arg2
     */
    public function thereIsALoanWithStartDateOfAndEndDateOf($arg1, $arg2)
    {
        $this->loan = new Loan(
            date_create_from_format('d/m/Y',$arg1),
            date_create_from_format('d/m/Y',$arg2)
        );
    }

    /**
     * @Given the loan has :arg1 tranches with the following details:
     */
    public function theLoanHasTranchesWithTheFollowingDetails($arg1, TableNode $table)
    {
        $hash = $table->getHash();
        $tranches = [];

        foreach ($hash as $row) {
            $tranches[] = new Tranche(
                $row['tranche_name'],
                Money::GBP($row['max_investment']),
                (int)$row['monthly_interest']
            );
        }
        $this->loan->setTranches($tranches);
    }

    /**
     * @Then I should see :arg1 invested on the tranche :arg2
     */
    public function iShouldSeeInvestedOnTheTranche($arg1, $arg2)
    {
        $tranche = $this->loan->getTranches()->getTrancheByName($arg2);
        \PHPUnit\Framework\Assert::assertTrue($tranche->getTotalAmountInvested()->equals(Money::GBP($arg1)));
    }


    /**
     * @Then investment should succeed
     */
    public function investmentShouldSucceed()
    {
        throw new PendingException();
    }

    /**
     * @When :arg1 invests £ :arg2  on the tranche :arg3 on :arg4
     */
    public function investsPsOnTheTrancheOn($arg1, $arg2, $arg3, $arg4)
    {
        $investor = $this->investors[$arg1];

        $loan = $this->loan;
        $tranche = $arg3;
        $amount = Money::GBP($arg2);
        $actionDate = date_create_from_format('d/m/Y',$arg4);

        $investor->invest($loan, $tranche, $amount, $actionDate);
    }

    /**
     * @When :arg1 tries to invest £ :arg2 on the tranche :arg3 on :arg4
     */
    public function triesToInvestPsOnTheTrancheOn($arg1, $arg2, $arg3, $arg4)
    {
        try {
            $investor = $this->investors[$arg1];
            $loan = $this->loan;
            $tranche = $arg3;
            $amount = Money::GBP($arg2);
            $actionDate = date_create_from_format('d/m/Y', $arg4);

            $investor->invest($loan, $tranche, $amount, $actionDate);
        } catch (Exception $e) {
            $this->outcome = $e;
        }
    }

    /**
     * @Then investment should fail with error message :arg1
     */
    public function investmentShouldFailWithErrorMessage($arg1)
    {
        \PHPUnit\Framework\Assert::assertSame($arg1, $this->outcome->getMessage());
    }

    /**
     * @When interest is calculated for the period :arg1 to :arg2
     */
    public function interestIsCalculatedForThePeriodTo($arg1, $arg2)
    {
        $dateFrom = date_create_from_format('d/m/Y',$arg1);
        $dateTo = date_create_from_format('d/m/Y',$arg2);
        $interestCalculator = new InterestCalculator($dateFrom, $dateTo, $this->loan);
        $interestCalculator->calculate();
    }

    /**
     * @Then :arg1 should have earned £ :arg2 interest
     */
    public function shouldHaveEarnedPsInterest($arg1, $arg2)
    {
        throw new PendingException();
    }
}
