Feature: Invest in a Loan
  In order to invest in a loan to earn interest
  As a investor
  I need to be able to invest in a loan

  Rules:
  - Each loan has a start date and an end date
  - Each loan is split in multiple tranches
  - Each tranche has a different monthly interest percentage
  - Each tranche has a maximum amount available to invest
  - When maximum amount invested in tranche, no further investments can be made
  - Investor can make investment in tranche at any time if the loan is still open, maximum of tranche has not been
  reached, and investor's wallet has sufficient balance
  - Interest due to be paid to investor is calculated end of the month

 Background:
   Given the following investors exist:
     | name       | virtual_wallet  | interest_earned |
     | Investor 1 | 1000            | 0               |
     | Investor 2 | 1000            | 0               |
     | Investor 3 | 1000            | 0               |
     | Investor 4 | 1000            | 0               |
   And there is a loan with start date of "01/10/2015" and end date of "15/11/2015"
   And the loan has 2 tranches with the following details:
     | tranche_name| monthly_interest| max_investment  |
     | A           | 3               | 1000            |
     | B           | 6               | 1000            |


 Scenario: Investing maximum amount pounds on the tranche "A" on "03/10/2015"
   When "Investor 1" invests £ "1000"  on the tranche "A" on "03/10/2015"
   Then  I should see "1000" invested on the tranche "A"

 Scenario: Investing in loan tranche "A" that has already reached maximum investment intake
   When "Investor 1" invests £ "1000"  on the tranche "A" on "03/10/2015"
   When "Investor 2" tries to invest £ "1" on the tranche "A" on "04/10/2015"
   Then investment should fail with error message "Exceeds Max Allowed Investment"

 Scenario: Investing amount less than the maximum allowed on loan tranche "B"
   When "Investor 3" invests £ "500"  on the tranche "B" on "03/10/2015"
   Then I should see "500" invested on the tranche "B"

 Scenario: Investing amount that exceeds the amount available in virtual wallet
   When "Investor 3" invests £ "500"  on the tranche "B" on "03/10/2015"
   When "Investor 4" tries to invest £ "1100" on the tranche "B" on "25/10/2015"
   Then investment should fail with error message "You have insufficient funds to make investment"

 Scenario: Calculating interest earned on loan for the month
   When "Investor 1" invests £ "1000"  on the tranche "A" on "03/10/2015"
   When "Investor 3" invests £ "500"  on the tranche "B" on "03/10/2015"
   When interest is calculated for the period "01/10/2015" to "31/10/2015"
   Then "Investor 1" should have earned £ "28.06" interest
   Then "Investor 3" should have earned £ "21.29" interest












