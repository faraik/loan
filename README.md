# Loan investment application

Defined requirements as understood from spec using behat feature scenarios which can be found here: features/loan.feature

## Getting started

1. Ensure you have composer installed
2. Run composer install `composer install`

## How to run the behat feature tests

 Run `vendor/bin/behat `
 
## How to run the unit tests

 Run `vendor/bin/phpunit `
 
### WIP ###

In various parts of the code I've highlighted with "TODO" the other things I would have liked to implement if I had more time


