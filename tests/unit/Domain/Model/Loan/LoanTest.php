<?php
namespace Loan\Tests\Domain\Model\Loan;

use InvalidArgumentException;
use Loan\Domain\Model\Loan\Loan;
use Loan\Domain\Model\Loan\Tranche;
use Money\Money;
use PHPUnit\Framework\TestCase;

class LoanTest extends TestCase
{
    // TODO :: More tests cases for expected exceptions and edge cases
    public function testCreateLoan()
    {
        $startDate = date_create_from_format('d/m/Y','01/10/2015');
        $endDate = date_create_from_format('d/m/Y','15/11/2015');

        $loan = new Loan($startDate, $endDate);

        $this->assertSame($startDate, $loan->getStartDate());
        $this->assertSame($endDate, $loan->getEndDate());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCreateLoanWithEndDateLessThanStartThrowsException()
    {
        $loan = new Loan(
            date_create_from_format('d/m/Y','01/10/2015'),
            date_create_from_format('d/m/Y','20/09/2015')
        );
    }

    public function testSetTranches()
    {
        $startDate = date_create_from_format('d/m/Y','01/10/2015');
        $endDate = date_create_from_format('d/m/Y','15/11/2015');

        $loan = new Loan($startDate, $endDate);

        $trancheA = new Tranche('A', Money::GBP(1000), 3);
        $trancheB = new Tranche('B', Money::GBP(1000), 6);

        $loan->setTranches([$trancheA, $trancheB]);

        // Retrieve stored trenches for the loan
        $tranches = $loan->getTranches();
        // Get trenches with matching name
        $storedTrancheA = $tranches->getTrancheByName($trancheA->getName());
        $storedTrancheB = $tranches->getTrancheByName($trancheB->getName());

        $this->assertSame($trancheA->getName(), $storedTrancheA->getName());
        $this->assertSame($trancheB->getName(), $storedTrancheB->getName());
    }

}