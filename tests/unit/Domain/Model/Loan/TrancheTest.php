<?php


namespace Loan\Tests\Domain\Model\Loan;


use InvalidArgumentException;
use Loan\Domain\Model\Loan\Tranche;
use Money\Money;
use PHPUnit\Framework\TestCase;

class TrancheTest extends TestCase
{
    // TODO :: More tests cases for expected exceptions and edge cases
    public function testCreateTranche()
    {
        /* @var Money $maximumInvestment  */
        $maximumInvestment = Money::GBP(1000);

        $monthlyInterest = 3;
        $tranche = new Tranche('A', $maximumInvestment, $monthlyInterest);

        $this->assertSame('A', $tranche->getName());
        $this->assertTrue($maximumInvestment->equals($tranche->getMaximumInvestment()));
        $this->assertSame($monthlyInterest, $tranche->getMonthlyInterest());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCreateTrancheWithEmptyNameThrowsException()
    {
        /* @var Money $maximumInvestment  */
        $maximumInvestment = Money::GBP(1000);

        $monthlyInterest = 3;
        $tranche = new Tranche('', $maximumInvestment, $monthlyInterest);
    }

    public function testTrancheCanProcessInvestmentRequest()
    {
        /* @var Money $maximumInvestment  */
        $maximumInvestment = Money::GBP(1000);
        $monthlyInterest = 3;
        $tranche = new Tranche('A', $maximumInvestment, $monthlyInterest);

        $amount = Money::GBP(1000);
        $actionDate = date_create_from_format('d/m/Y','03/10/2015');
        $tranche->processInvestment($amount, 'Investor 1', $actionDate);

        $this->assertTrue($tranche->getTotalAmountInvested()->equals($amount));
    }
}