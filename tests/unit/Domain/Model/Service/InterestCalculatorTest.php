<?php

namespace Loan\Tests\Domain\Model\Service;

use Loan\Domain\Model\Investor\Investor;
use Loan\Domain\Model\Investor\VirtualWallet;
use Loan\Domain\Model\Loan\Loan;
use Loan\Domain\Model\Loan\Tranche;
use Loan\Domain\Model\Service\InterestCalculator;
use Money\Money;
use PHPUnit\Framework\TestCase;

class InterestCalculatorTest extends TestCase
{
    private $loan;

    /**
     * @var Investor
     */
    private $investor1;

    /**
     * @var Investor
     */
    private $investor2;

    public function setUp()
    {
        $this->loan = $this->createLoan();
        $this->investor1 = $this->setUpInvestor('Investor1');
        $this->investor2 = $this->setUpInvestor('Investor2');
    }

    public function testInterestCalculator()
    {
        $dateFrom = date_create_from_format('d/m/Y','01/10/2015');
        $dateTo = date_create_from_format('d/m/Y','31/10/2015');

        $trancheA = 'A';
        $trancheB = 'B';
        $investor1amount = Money::GBP(1000);
        $investor1ActionDate = date_create_from_format('d/m/Y','03/10/2015');

        $investor2amount = Money::GBP(500);
        $investor2ActionDate = date_create_from_format('d/m/Y','10/10/2015');

        $this->investor1->invest($this->loan, $trancheA,  $investor1amount, $investor1ActionDate);
        $this->investor2->invest($this->loan, $trancheB, $investor2amount, $investor2ActionDate);

        $interestCalculator = new InterestCalculator($dateFrom, $dateTo, $this->loan);

        $interestCalculator->calculate();

        $this->assertTrue(false);

       // $this->assertTrue($this->investor1->getInterestEarned()->equals(Money::GBP('28.06')));
       // $this->assertTrue($this->investor2->getInterestEarned()->equals(Money::GBP('21.29')));
    }

    public function createLoan()
    {
        $startDate = date_create_from_format('d/m/Y','01/10/2015');
        $endDate = date_create_from_format('d/m/Y','15/11/2015');

        $loan = new Loan($startDate, $endDate);

        $trancheA = new Tranche('A', Money::GBP(1000), 3);
        $trancheB = new Tranche('B', Money::GBP(1000), 6);

        $loan->setTranches([$trancheA, $trancheB]);

        return $loan;
    }

    public function setUpInvestor(string $name)
    {
        // Set up new Investor with £1000 balance
        $virtualWallet = VirtualWallet::fromString('1000');
        $interestEarned = Money::GBP(0);

        return new Investor($name, $virtualWallet, $interestEarned);
    }

}