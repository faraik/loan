<?php

namespace Loan\Tests\Domain\Model\Investor;

use InvalidArgumentException;
use Loan\Domain\Model\Investor\Investor;
use Loan\Domain\Model\Investor\VirtualWallet;
use Loan\Domain\Model\Loan\Loan;
use Loan\Domain\Model\Loan\Tranche;
use Money\Money;

class InvestorTest extends \PHPUnit\Framework\TestCase
{
    // TODO :: More tests cases for expected exceptions and edge cases
    public function testCreateInvestor()
    {
        $virtualWallet = VirtualWallet::fromString('1');
        $interestEarned = Money::GBP(1);

        $investor = new Investor('Investor1', $virtualWallet, $interestEarned);

        $this->assertSame('Investor1', $investor->getName());
        $this->assertInstanceOf(VirtualWallet::class, $investor->getVirtualWallet());
        $this->assertInstanceOf(Money::class, $investor->getInterestEarned());
    }

    public function testInvestorCanMakeInvestment()
    {
        // Set up new loan with Tranche A
        $startDate = date_create_from_format('d/m/Y','01/10/2015');
        $endDate = date_create_from_format('d/m/Y','15/11/2015');

        $loan = new Loan($startDate, $endDate);
        $trancheA = new Tranche('A', Money::GBP(1000), 3);

        $loan->setTranches([$trancheA]);

        // Set up new Investor with £1000 balance
        // TODO:: Implement VirtualWallet::fromAmount()
        $virtualWallet = VirtualWallet::fromString('1000');
        $interestEarned = Money::GBP(0);

        $tranche = 'A';
        $amount = Money::GBP(1000);
        $actionDate = date_create_from_format('d/m/Y','03/10/2015');
        $investor = new Investor('Investor1', $virtualWallet, $interestEarned);
        $result = $investor->invest($loan, $tranche, $amount, $actionDate);
        $this->assertTrue($result);
        $this->assertTrue($investor->getVirtualWallet()->getBalance()->equals(Money::GBP(0)));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCreateInvestorWithEmptyNameThrowsException()
    {
        $virtualWallet = VirtualWallet::fromString('1');
        $interestEarned = Money::GBP(1);

        $investor = new Investor('', $virtualWallet, $interestEarned);
    }
}